Introduction to Git

VCS/Version control system
    -VCS is a piece of software that manages/controls the revisions of a source code/document/collection of information
    -This allows the programmer/developer to create revisions 

What is Git?
      -Git is an open source VCS that we use to track changes in our files within a repository
Repositories
     Local repositories - initialized folders that use git
                        - therefore, it allows us to track changes made in the files with the folder
                        - these changes can be then uploaded in the remote repositories



SSH key
    Secure shell key    - used to authenticate the uploading/pushing or
    doing other tasks then manipulating or using git repositories
                        - passwords will not be required once SSH keys are set

Generating SSH keys and configure Machine (one time setup for new machines)

-geberates SSH key

ssh-keygen

-press enter key 3 times

-copy ssh key (to put it in the clipboard)

Mac
pbcopy < ~/.ssh/id_rsa.pub

windows
cat ~/.ssh/id_rsa.pub | clip


Git Basics Commands

1. git init
- this command is used to prepare and set your local repository
-A .git folder will be created inside the current directory
NOTE: You will not be able to see this folder because it is set to hidden.

2. git status
- this command will displaty all updates that are not yet saved to the latest commit version of the project
-lets you see which changes have been staged, which haven't, and which files aren't being tracked

3. git add
- this command will add or stage the files preparing them to be included in the next commit/snapshot of the project
-prepares the file or set of files into the staging area, where the files are eventually added to the recorded history of changes

4. git commit -m "[commitMessage]"
-saves the changes added from git add . to its recorded history of changes or commits
-Example:
  git commit -m "Initial Commit"
 -"initial commit" is used in the message to help developers identify the first commit.
 -succeeding commit messages should be descriptive of the changes to the project

 5. git log
 -displays the history of changes for the repo
 -git log  --oneline will simplify the display output

 CONNECT LOCAL GIT REPO TO REMOTE GIT REPO

Note: Makes sure that you have already creatd your repository in Gitlab

git remote add origin git@gitlab.com:batch-248-dimapilis/s02.git
git push origin mastergi



git@gitlab.com:batch-248-dimapilis/s02.git
















     



